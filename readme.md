# Kubernetes Deployment Solution

# This project outlines a Kubernetes deployment solution to deploy two containers that scale independently from each other. 
# The application is package with Node.js, cross-platform Javascripts runtime environment, from this single package to independent images were created resulting in the creation of two containers
# The first container runs code that runs a small API that returns users from a database, while the second container runs code that runs a small API that returns shifts from a database.
# The solution also ensures that the deployment can handle rolling deployments and rollbacks

Features:
- Two containers that scale independently from each other
- Daily bell-curve scaling
- Rolling deployments and rollbacks
- RBAC to limit development team commands

Requirements:
- Kubernetes cluster
- Docker
- kubectl

Usage:
1. Run `kubectl apply -f deployment.yaml` to create the deployment.
2. Run `kubectl apply -f service.yaml` to create the service.
3. Run `kubectl apply -f hpa.yaml` to create the horizontal pod autoscaler.
4. Verify that the pods are running with `kubectl get pods`.
5. Verify that the services are running with `kubectl get services`.
7. Another way is to add skaffold.yaml file and use 'skaffold run' to carryout numbers 1-3.

RBAC:
The RBAC policy allows the development team to deploy and roll back the application, but restricts them from running certain commands on the Kubernetes cluster.

Bonus:
Multiple Environments:
To apply the configs to multiple environments, create separate Kubernetes configuration files for each environment (e.g. staging.yaml, production.yaml) and use the appropriate file for each environment. Another way is to varibalize deployment.yaml, hpa.yaml and services.yaml using Helm Chart. Next step is to values i.e. staging and production environment then inject the values into kubernetes resources after which use helm to create those resources using the injected values.

Network Latency Scaling:
To auto-scale the deployment based on network latency instead of CPU, create a custom metrics API that measures network latency and use the Kubernetes metrics server to autoscale based on those metrics.

RESOURCES
1. https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
2. https://kubernetes.io/docs/concepts/services-networking/service/
3. https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/#create-horizontal-pod-autoscaler
4. https://kubernetes.io/docs/concepts/configuration/secret/#secret-types
5. https://kubernetes.io/docs/reference/access-authn-authz/rbac/

